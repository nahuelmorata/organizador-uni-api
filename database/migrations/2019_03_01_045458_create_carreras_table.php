<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrerasTable extends Migration {
    public function up() {
        Schema::create('carrerasOrganizadorUni', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });
    }

    public function down() {
        Schema::dropIfExists('carrerasOrganizadorUni');
    }
}
