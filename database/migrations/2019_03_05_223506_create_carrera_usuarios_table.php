<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarreraUsuariosTable extends Migration {
    public function up() {
        Schema::create('carreraUsuariosOrganizadoruUni', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_usuario');
            $table->unsignedInteger('id_carrera');

            $table->foreign('id_usuario')->references('id')->on('cuentasOrganizadorUni');
            $table->foreign('id_carrera')->references('id')->on('carrerasOrganizadorUni');
        });
    }

    public function down() {
        Schema::dropIfExists('carreraUsuariosOrganizadoruUni');
    }
}
