<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration {
    public function up() {
        Schema::create('cuentasOrganizadorUni', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario');
            $table->string('password');
            $table->string('email');
            $table->string('token');
        });
    }

    public function down() {
        Schema::dropIfExists('cuentasOrganizadorUni');
    }
}
