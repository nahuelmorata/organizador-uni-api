<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasTable extends Migration {
    public function up(){
        Schema::create('materiasOrganizadorUni', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->unsignedInteger('id_carrera');

            $table->foreign('id_carrera')->references('id')->on('carrerasOrganizadorUni');
        });
    }

    public function down() {
        Schema::dropIfExists('materiasOrganizadorUni');
    }
}
