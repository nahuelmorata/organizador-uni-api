<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriaCursandosTable extends Migration {
    public function up() {
        Schema::create('materiasCursandoOrganizadorUni', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_cuenta');
            $table->unsignedInteger('id_materia');
            $table->text('data_cursado');
            $table->text('data_promocion');

            $table->foreign('id_cuenta')->references('id')->on('cuentasOrganizadorUni');
            $table->foreign('id_materia')->references('id')->on('materiasOrganizadorUni');
        });
    }

    public function down() {
        Schema::dropIfExists('materiasCursandoOrganizadorUni');
    }
}
