<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrelativasTable extends Migration {
    public function up() {
        Schema::create('correlativasOrganizadorUni', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_materia');
            $table->unsignedInteger('id_materia_correlativa');
            $table->unsignedInteger('estado');
            $table->unsignedInteger('condicion');

            $table->foreign('id_materia')->references('id')->on('materiasOrganizadorUni');
            $table->foreign('id_materia_correlativa')->references('id')->on('materiasOrganizadorUni');
        });
    }

    public function down() {
        Schema::dropIfExists('correlativasOrganizadorUni');
    }
}
