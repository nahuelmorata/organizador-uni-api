<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correlativa extends Model {
    public $timestamps = false;
    protected $table = "correlativasOrganizadorUni";

    public function materia() {
        return $this->belongsTo('App\Materia', 'id_materia');
    }
}
