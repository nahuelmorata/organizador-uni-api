<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model {
    public $timestamps = false;
    public $table = "cuentasOrganizadorUni";

    protected $attributes = [
        'token' => ''
    ];

    public function carreras() {
        return $this->hasManyThrough('App\Carrera', 'App\CarreraUsuario', 'id_carrera', 'id', 'id', 'id_usuario');
    }

    public function materiasCursando() {
        return $this->hasMany('App\MateriaCursando', 'id_materia');
    }
}
