<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarreraUsuario extends Model {
    protected $table = "carreraUsuariosOrganizadoruUni";
    public $timestamps = false;

    public function usuario() {
        return $this->belongsTo('App\Cuenta', 'id_usuario');
    }
}
