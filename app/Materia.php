<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model {
    public $timestamps = false;
    protected $table = "materiasOrganizadorUni";

    public function carrera() {
        return $this->belongsTo('App\Carrera', 'id_carrera');
    }

    public function correlativas() {
        return $this->hasMany('App\Correlativa', 'id_materia');
    }
}
