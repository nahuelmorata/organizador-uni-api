<?php

namespace App\GraphQL\Query;

use App\Carrera;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;

class CarreraQuery extends Query {
    protected $attributes = [
        'name' => 'CarreraQuery',
        'description' => 'Query de Carrera'
    ];

    public function type() {
        return Type::listOf(GraphQL::type('carrera'));
    }

    public function args() {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'Id de la carrera'
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $with = $fields->getRelations();
        $select = $fields->getSelect();

        if (isset($args['id'])) {
            return Carrera::where('id', '=', $args['id'])->get();
        }

        return Carrera::with($with)->select($select)->get();
    }
}
