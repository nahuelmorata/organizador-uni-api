<?php

namespace App\GraphQL\Query;

use App\Cuenta;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;

class CuentaQuery extends Query {
    protected $attributes = [
        'name' => 'CuentaQuery',
        'description' => 'A query'
    ];

    public function type() {
        return GraphQL::type('cuentaQuery');
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token de cuenta'
            ]
        ];
    }

    public function rules(array $args = []) {
        return [
            'token' => [ 'required' ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $cuenta = Cuenta::where('token', '=', $args['token'])->get();

        if (count($cuenta) != 0) {
            return $cuenta[0];
        }

        return [];
    }
}
