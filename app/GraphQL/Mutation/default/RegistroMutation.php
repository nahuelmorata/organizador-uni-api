<?php

namespace App\GraphQL\Mutation;

use App\Cuenta;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class RegistroMutation extends Mutation {
    protected $attributes = [
        'name' => 'RegistroMutation',
        'description' => 'Registro de cuenta'
    ];

    public function type() {
        return Type::listOf(Type::string());
    }

    public function args() {
        return [
            'usuario' => [
                'type' => Type::string(),
                'description' => 'Usuario para la cuenta'
            ],
            'password' => [
                'type' => Type::string(),
                'description' => 'Contraseña de la cuenta'
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'Email de la cuenta'
            ]
        ];
    }

    public function rules(array $args = []) {
        return [
            'usuario' => ['required'],
            'password' => ['required'],
            'email' => ['required']
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        if ($this->existeEmail($args['email'])) {
            return ['El mail ya existe'];
        }

        if ($this->existeUsuario($args['usuario'])) {
            return ['El usuario ya existe'];
        }

        $cuenta = new Cuenta;

        $cuenta->usuario = $args['usuario'];
        $cuenta->password = password_hash($args['password'], PASSWORD_DEFAULT);
        $cuenta->email = $args['email'];

        $cuenta->save();

        return [];
    }

    private function existeEmail($email) {
        $cuenta = Cuenta::where('email', '=', $email)->get();

        return count($cuenta) != 0;
    }

    private function existeUsuario($usuario) {
        $cuenta = Cuenta::where('usuario', '=', $usuario)->get();

        return count($cuenta) != 0;
    }
}
