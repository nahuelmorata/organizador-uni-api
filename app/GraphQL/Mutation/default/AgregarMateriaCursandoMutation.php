<?php

namespace App\GraphQL\Mutation;

use App\Cuenta;
use App\MateriaCursando;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class AgregarMateriaCursandoMutation extends Mutation {
    protected $attributes = [
        'name' => 'AgregarMateriaCursandoMutation',
        'description' => 'Mutation para agregar una materia a cursar'
    ];

    public function type() {
        return Type::string();
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token de la cuenta',
                'rules' => ['required']
            ],
            'materia' => [
                'type' => Type::int(),
                'description' => 'Id de la materia',
                'rules' => ['required']
            ],
            'dataCursado' => [
                'type' => Type::string(),
                'description' => 'Datos del cursado de la materia',
                'rules' => ['required']
            ],
            'dataPromocion' => [
                'type' => Type::string(),
                'description' => 'Datos de la promocion de la materia',
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $cuenta = Cuenta::where('token', '=', $args['token'])->get();

        if (isset($cuenta[0])) {
            $materiaCursandoVerificacion = MateriaCursando::where('id_cuenta', '=', $cuenta[0]->id)->where('id_materia', '=', $args['materia'])->get();

            if (!isset($materiaCursandoVerificacion[0])) {
                $materiaCursando = new MateriaCursando;

                $materiaCursando->id_cuenta = $cuenta[0]->id;
                $materiaCursando->id_materia = $args['materia'];
                $materiaCursando->data_cursado = $args['dataCursado'];
                $materiaCursando->data_promocion = $args['dataPromocion'];

                $materiaCursando->save();

                return "";
            } else {
                return "No puede cursar a la vez la misma materia";
            }
        } else {
            return "Inicie sesion antes";
        }
    }
}
