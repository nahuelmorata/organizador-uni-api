<?php

namespace App\GraphQL\Mutation;

use App\Cuenta;
use App\MateriaCursando;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class ModificarDatosPromocionMutation extends Mutation {
    protected $attributes = [
        'name' => 'ModificarDatosPromocionMutation',
        'description' => 'Mutation para modificar los datos de la promocion'
    ];

    public function type() {
        return Type::string();
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token de la cuenta',
                'rules' => ['required']
            ],
            'materia' => [
                'type' => Type::int(),
                'description' => 'Id de la materia',
                'rules' => ['required']
            ],
            'dataPromocion' => [
                'type' => Type::string(),
                'description' => 'Datos de la promocion',
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $cuenta = Cuenta::where('token', '=', $args['token'])->get();

        if (isset($cuenta[0])) {
            $materiaCursando = MateriaCursando::where('id_cuenta', '=', $cuenta[0]->id)->where('id_materia', '=', $args['materia'])->get();

            if ($materiaCursando[0]) {
                $materiaCursando[0]->data_promocion = $args['dataPromocion'];

                $materiaCursando[0]->save();

                return "";
            } else {
                return "No esta cursando esa materia";
            }
        } else {
            return "Primero inicie sesion";
        }
    }
}
