<?php

namespace App\GraphQL\Mutation;

use App\Cuenta;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class LoginMutation extends Mutation {
    protected $attributes = [
        'name' => 'LoginMutation',
        'description' => 'Login para cuentas'
    ];

    public function type() {
        return GraphQL::type('cuentaMutation');
    }

    public function args() {
        return [
            'usuario' => [
                'name' => 'usuario',
                'type' => Type::string()
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::string()
            ]
        ];
    }

    public function rules(array $args = []) {
        return [
            'usuario' => ['required'],
            'password' => ['required']
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $cuenta = Cuenta::where('usuario', '=', $args['usuario'])->get();

        if (count($cuenta) != 0 && $this->verificarPassword($args['password'], $cuenta[0]['password'])) {
            $cuenta = Cuenta::find($cuenta[0]['id']);

            $cuenta->token = bin2hex(random_bytes(127));

            $cuenta->save();

            return [
                'id' => $cuenta->id,
                'token' => $cuenta->token
            ];
        } else {
            return [
                'error' => 'Usuario o contraseña incorrecta'
            ];
        }
    }

    private function verificarPassword($passwordIngresada, $passwordGuardada) {
        return password_verify($passwordIngresada, $passwordGuardada);
    }
}
