<?php

namespace App\GraphQL\Mutation;

use App\CarreraUsuario;
use App\Cuenta;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class AgregarCarreraCuentaMutation extends Mutation {
    protected $attributes = [
        'name' => 'AgregarCarreraCuentaMutation',
        'description' => 'Mutation para agregar una carrera a una cuenta'
    ];

    public function type() {
        return Type::string();
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token del usuario',
                'rules' => ['required']
            ],
            'carrera' => [
                'type' => Type::int(),
                'description' => 'Id de la carrera',
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        $usuario = Cuenta::where('token', '=', $args['token'])->get()[0];

        $carreraUsuarioVerificacion = CarreraUsuario::where('id_usuario', '=', $usuario['id'])->where('id_carrera', '=', $args['carrera'])->get();

        if (!isset($carreraUsuarioVerificacion[0])) {
            $carreraUsuario = new CarreraUsuario;

            $carreraUsuario->id_usuario = $usuario['id'];
            $carreraUsuario->id_carrera = $args['carrera'];

            $carreraUsuario->save();

            return "";
        }

        return "La carrera ya esta agregada";
    }
}
