<?php

namespace App\GraphQL\Mutation;

use App\CarreraUsuario;
use App\Cuenta;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class BorrarCarreraCuentaMutation extends Mutation {
    protected $attributes = [
        'name' => 'BorrarCarreraCuentaMutation',
        'description' => 'Mutation para borar una carrera de una cuenta'
    ];

    public function type() {
        return Type::string();
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token del usuario',
                'rules' => ['required']
            ],
            'carrera' => [
                'type' => Type::int(),
                'description' => 'Id de la carrera',
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $usuario = Cuenta::where('token', '=', $args['token'])->get()[0];

        $carreraUsuario = CarreraUsuario::where('id_usuario', '=', $usuario['id'])->where('id_carrera', '=', $args['carrera'])->get();

        if (isset($carreraUsuario[0])) {
            $carreraUsuario[0]->delete();

            return "";
        }

        return "La usuario no esta en esa carrera";
    }
}
