<?php

namespace App\GraphQL\Mutation;

use App\Cuenta;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CerrarSesionMutation extends Mutation {
    protected $attributes = [
        'name' => 'CerrarSesionMutation',
        'description' => 'Mutacion para cerrar sesion'
    ];

    public function type() {
        return Type::string();
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token de la cuenta',
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $cuenta = Cuenta::where('token', '=', $args['token'])->get();

        if (isset($cuenta[0])) {
            $cuenta[0]->token = "";
            $cuenta[0]->save();
        }

        return "Primero inicie sesion";
    }
}
