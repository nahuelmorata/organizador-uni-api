<?php

namespace App\GraphQL\Mutation;

use App\Cuenta;
use App\MateriaCursando;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class BorrarMateriaCursandoMutation extends Mutation {
    protected $attributes = [
        'name' => 'BorrarMateriaCursandoMutation',
        'description' => 'Mutation para borrar una carrera cursando'
    ];

    public function type() {
        return Type::string();
    }

    public function args() {
        return [
            'token' => [
                'type' => Type::string(),
                'description' => 'Token de la cuenta',
                'rules' => ['required']
            ],
            'materia' => [
                'type' => Type::int(),
                'description' => 'Id de la materia',
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info) {
        $cuenta = Cuenta::where('token', '=', $args['token'])->get();

        if (isset($cuenta[0])) {
            $materiaCursando = MateriaCursando::where('id_materia', '=', $cuenta[0]->id)->where('id_materia', '=', $args['materia'])->get();

            if (isset($materiaCursando[0])) {
                $materiaCursando[0]->delete();

                return "";
            } else {
                return "Primero curse la materia";
            }
        } else {
            return "Inice sesion primero";
        }
    }
}
