<?php

namespace App\GraphQL\Type;

use App\Carrera;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CarreraType extends GraphQLType {
    protected $attributes = [
        'name' => 'CarreraType',
        'description' => 'Tipo de una carrera',
        'model' => Carrera::class
    ];

    public function fields() {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'Id de la carrera'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre de la carrera'
            ],
            'materias' => [
                'type' => Type::listOf(GraphQL::type('materia')),
                'description' => 'Materias de la carrera'
            ]
        ];
    }
}
