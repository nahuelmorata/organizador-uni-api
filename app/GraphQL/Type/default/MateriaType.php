<?php

namespace App\GraphQL\Type;

use App\Materia;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MateriaType extends GraphQLType{
    protected $attributes = [
        'name' => 'MateriaType',
        'description' => 'Tipo de una materia',
        'model' => Materia::class
    ];

    public function fields() {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'Id de la materia'
            ],
            'nombre' => [
                'type' => Type::string(),
                'description' => 'Nombre de la materia'
            ],
            'correlativas' => [
                'type' => Type::listOf(GraphQL::type('correlativa')),
                'description' => 'Correlativas de la materia',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->correlativas;
                }
            ]
        ];
    }
}
