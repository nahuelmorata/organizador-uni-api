<?php

namespace App\GraphQL\Type;

use App\Correlativa;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CorrelativaType extends GraphQLType {
    protected $attributes = [
        'name' => 'CorrelativaType',
        'description' => 'Tipo de una correlativa',
        'model' => Correlativa::class
    ];

    public function fields() {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'Id de la correlativa'
            ],
            'materia' => [
                'type' => GraphQL::type('materia'),
                'description' => 'Materia correlativa',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->materia;
                }
            ],
            'estado' => [
                'type' => Type::int(),
                'description' => 'Estado a cumplir de la materia'
            ],
            'condicion' => [
                'type' => Type::int(),
                'description' => 'Condicion a cumplir de la materia correlativa'
            ]
        ];
    }
}
