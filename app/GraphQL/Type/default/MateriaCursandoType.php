<?php

namespace App\GraphQL\Type;

use App\MateriaCursando;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MateriaCursandoType extends GraphQLType {
    protected $attributes = [
        'name' => 'MateriaCursandoType',
        'description' => 'Tipo de una materia en cursado',
        'model' => MateriaCursando::class
    ];

    public function fields() {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'Id de la Materia'
            ],
            'materia' => [
                'type' => GraphQL::type('materia'),
                'description' => 'Materia que esta cursando',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->materia;
                }
            ],
            'dataCursado' => [
                'type' => Type::string(),
                'description' => 'Datos del cursado',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->data_cursado;
                }
            ],
            'dataPromocion' => [
                'type' => Type::string(),
                'description' => 'Datos de la promocion',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->data_promocion;
                }
            ]
        ];
    }
}
