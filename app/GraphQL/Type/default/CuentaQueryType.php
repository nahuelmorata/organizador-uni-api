<?php

namespace App\GraphQL\Type;

use App\Cuenta;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CuentaQueryType extends GraphQLType {
    protected $attributes = [
        'name' => 'CuentaQueryType',
        'description' => 'Tipo de una cuenta para query',
        'model' => Cuenta::class
    ];

    public function fields() {
        return [
            'email' => [
                'type' => Type::string(),
                'description' => 'Email de la cuenta'
            ],
            'carreras' => [
                'type' => Type::listOf(GraphQL::type('carrera')),
                'description' => 'Carreras del usuario',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->carreras;
                }
            ],
            'materiaCursando' => [
                'type' => Type::listOf(GraphQL::type('materiaCursando')),
                'description' => 'Materia cursando',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->materiasCursando;
                }
            ]
        ];
    }
}
