<?php

namespace App\GraphQL\Type;

use App\Cuenta;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CuentaMutationType extends GraphQLType {
    protected $attributes = [
        'name' => 'CuentaMutationType',
        'description' => 'Tipo de una cuenta para mutation',
        'model' => Cuenta::class
    ];

    public function fields() {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'Id de la cuenta'
            ],
            'token' => [
                'type' => Type::string(),
                'description' => 'Token si login correcto sino vacio',
                'selectable' => false
            ],
            'error' => [
                'type' => Type::string(),
                'description' => 'Mensaje de error',
                'selectable' => false
            ]
        ];
    }
}
