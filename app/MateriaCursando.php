<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriaCursando extends Model {
    public $timestamps = false;
    protected $table = "materiasCursandoOrganizadorUni";

    protected $attributes = [
        'data_cursado' => '{}',
        'data_promocion' => '{}'
    ];

    public function cuenta() {
        return $this->belongsTo('App\Cuenta', 'id_materia');
    }

    public function materia() {
        return $this->belongsTo('App\Materia', 'id_materia');
    }
}
