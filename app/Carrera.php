<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model {
    public $timestamps = false;
    protected $table = "carrerasOrganizadorUni";

    public function materias() {
        return $this->hasMany('App\Materia', 'id_carrera');
    }
}
